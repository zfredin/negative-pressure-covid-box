# Negative Pressure Covid Box
This to-be-better-named project is an isolation box that prevents aerosols from spreading beyond the patient's immediate area. It allows caregivers to use currently-prohibited respiratory aides, such as CPAP, BiPAP, and nebulizers. The first physical prototype uses a modified version of our [Covid Isolation Box](https://gitlab.cba.mit.edu/alfonso/covid-isolation-box), now equipped with a fancy cupola (images include protective film for clarity; in use, the box is transparent):


Table of Contents:
- [Horizontal Patient Negative Pressure Box](horizontal.md)
- [Seated - Walking Negative Pressure Box](vertical.md)
- [Electronics](electronics.md)
- [Design Needs // Feedback](feedback.md)


![fanprototype1](img/main.png)
