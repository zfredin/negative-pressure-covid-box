## Background
In early March of 2020, our group at the CBA started a number of [rapid coronavirus response projects](https://gitlab.cba.mit.edu/pub/coronavirus/tracking). We brought together a range of subject matter experts from a variety of disciplines, including regulatory agencies, medical professionals, airflow modeling specialists, and supply chain managers. Our hope was to quickly learn what gaps existed in the worldwide virus response and respond by quickly prototyping solutions before spinning the concepts off to traditional vendors or the distributed FabLab network for manufacturing and scaling. One early success came about in caregiver PPE, where we helped the Project Manus team rapidly prototype and evaluate their [folded plastic faceshield](https://project-manus.mit.edu/fs). This project built on our earlier tests of [crowdsourced face shield designs](https://gitlab.cba.mit.edu/zfredin/quickshield) and spawned an [open-source version](https://gitlab.cba.mit.edu/alfonso/fabshield) designed for bulk fabrication using FabLab equipment.

During the course of our collaboration with several local medical institutions, we learned that patient-centric personal protective equipment (PPE) could provide a great deal of value by reducing infection risk for the myriad of specialists that treat a typical covid-19 patient. We started by exploring altenatives to Dr. Hsien Yung Lai's [Aerosol Box](https://sites.google.com/view/aerosolbox/design), eventually designing and field-testing a [folded version](https://gitlab.cba.mit.edu/alfonso/covid-isolation-box) made out of a single sheet of low-cost plastic.  Subsequent discussions suggested that a version capable of containing aerosols in addition to sprayed bodily fluids would go much further, as it would unlock caregivers' ability to use non-invasive ventilation techniques such as BiPAP, CPAP, and nebulizers.

## Study
This independent study proposes exploring the design, simulation, prototyping, and early trials of a negative pressure patient isolation box. In particular, the work described here will focus on empircally validating the simulated airflow model using a combination of differential pressure instrumentation, pitot-tube velocity measurement, and smoke wand airflow visualization. The study will also examine the feasibility of using online differential pressure-based control along with a set of actuated louvres to maintain flow velocity through the filtration system. The design will also be evaluated with an eye towards scaling manufacturing, both via conventional means and through the global distributed FabLab network.

Crucially, a major learning from this project will be managing effective collaboration across disciplines with a remotely located project team, including:
- Alfonso Rubio, folded box design
- Erik Strand, Jonathan Jilesen, airflow simulation
- Zach Fredin, system integration and instrumentation
- Elazer Edelman, Zaid Altawil, Willie Baker, Eric Goralnick, clinical trials and testing
- Gordon Sharp, design consultation

A significant amount of work has already gone into this project, including a thorough distillation of requirements from numerous interviews with a variety of stakeholders.

## References
EPA HEPA reference/fact sheet: https://www3.epa.gov/ttncatc1/dir1/ff-hepa.pdf
Existing designs, commercial and otherwise:
- [Harvard GSD](https://www.gsd.harvard.edu/2020/04/gsd-begins-patient-isolation-hood-pih-design-and-fabrication-alongside-ongoing-ppe-efforts/)
- [PreVent project](https://drive.google.com/drive/folders/1IQj04TAOrpxp02VxtnU-UU3iDlftDINP)
- [design from Prof. Yochai Adir](https://www.sciencecodex.com/cost-effective-canopy-protects-health-workers-covid-infection-during-ventilation-645350)
- [NIOSH/CDC report](https://www.cdc.gov/niosh/surveyreports/pdfs/301-05f.pdf)
- [Peace Medical Demistifier 2000 (great name)](https://peacemedical.com/demist2000.htm)

## Evaluation
The effort will be evaluated based on a few criteria:
- acceptance by the clinical trial/testing group
- agreement between simulation and testing
- meeting requirements from early tests and interviews
- success building and fielding a prototype

This information will be presented in a final report with clear next steps.
