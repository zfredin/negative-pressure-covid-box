## Need.

As discussed in an [issue I posted](https://gitlab.cba.mit.edu/pub/coronavirus/tracking/-/issues/38) in mid-April, covid-19 has dramatically curtailed the support toolkit available to respiratory specialists. To recap:

>Briefly, hospitals have updated protocols to essentially ban these common respiratory interventions on any patient that presents with COVID-19 symptoms to avoid putting hospital staff at risk. Eric said that currently ~10% of these patients end up testing positive; the remaining 90% are thus being deprived critical treatment for COPD, congestive heart failure, and many other ailments. He believes in the short and long-term such a design could be a game-changer, since it frees up PPE, reduces caregiver risk, and opens up mid-range treatment options prior to pushing doctors to rely on invasive ventilation.

In short, a system that makes BiPAP, CPAP, and nebulizers viable again has great deal of leverage:
- reduced pressure on dwindling ventilator stockpiles
- better patient outcomes through less-invasive interventions
- less need for PPE during non-physical patient interactions
- improved caregiver safety due to reduced aerosol contamination risk

## design consultation
Gordon Sharp (chairman of Aircuity / visual artist / fume hood design veteran) and Willie Baker, MD (BMC) provided many useful comments they permitted me to reproduce here:

>  Zach:
>
> Let me know if I can provide any more information on the fan filter unit or provide any advice on enclosure containment for the project.
>
> By way of additional background, besides my airflow based kinetic sculpture work, I am also an MIT graduate, class of ’77 and founded Phoenix Controls back in the 80’s which is still one of the world leaders in both hospital isolation room containment controls as well as lab airflow controls to both vary the airflow volumes of fume hoods to maintain containment and save energy as well as control lab room pressurization through room supply and exhaust air control. In fact, most of MIT’s fume hoods and lab rooms use Phoenix Controls’ lab airflow controls equipment. In 2000, I started Aircuity that involves monitoring air quality such as CO2, particulates and VOC’s in both labs, hospitals and commercial facilities to provide superior safe and healthy environments while still saving significant amounts of energy. I have Aircuity equipment at MIT as well for example. Finally, I am also a voting member of ASHRAE ANSI standard 170 on Healthcare Ventilation that is used to set the state codes on ventilation rates in hospitals across the US as well as an ASHRAE Distinguished Lecturer and a member of ASHRAE technical committees on cleanrooms and labs.
>
> My knowledge although reasonably deep in some areas is also pretty narrow in scope, but I do have some industry contacts that might be helpful. It sounds like you have done some good CFD work already, however if you would benefit from some assistance or someone to bounce some ideas off of in this area, I know a CFD expert, Dr. Kishor Khankari, who is quite knowledgeable in the area of airflow and containment and might be very willing to donate some of his time to this cause.
>
> One comment from my direct experience is that you probably don’t need a pressure sensor for control of the airflow nor a high pressure drop. First of all, you shouldn’t need to much pressure difference, a few hundredths of an inch will be more than adequate. Typically for containment we would need only 100 fpm or more of velocity through any openings to maintain containment which would correspond to a pressure drop of only 0.0006”. If you had some heavy breathing from a patient that was not wearing a mask, perhaps 400 fpm or 0.01” of water could be used, but there is diminishing returns here. As the velocity gets above a few hundred fpm you can start to create turbulence and eddies that can actually create a loss of containment. Thus, even though you might think more is better this is actually not necessarily true particularly when you do not have aerodynamically shaped openings. With some of the plastic sheeting or flaps there will be sharp edges that can create eddies near the edges that can suck vapors out of the enclosure. There is for example a lot of fume hood containment data on this and that is why fume hoods have optimum operation at around 100 fpm face velocity. If we had a biocontainment room like a BL3 containment facility, that would be a different story with higher pressure levels but also a near airtight enclosure.
>
> In terms of pressure control, I would not recommend controlling pressure as it will be very difficult to make a stable system that can respond fast enough to maintain containment. First of all you will be working with very low pressure differences that are not easy to sense and will be very “noisy”. Second the fans of the type we have been discussing do not change speed quickly. To ramp them up or down takes 5 to over 10 seconds. This will create control lags that will mean the system has to be slowed down in its response time in order to be stable and not oscillate or overshoot. On the other hand, when a flap is opened you cannot wait for the system to respond in 10 seconds as you will lose containment. I had this problem with controlling the airflow and face velocity in fume hoods. To be successful we went away from measuring the pressure in the hood and went to a direct proportional airflow volume control based on the measurement of sash position or the open area of the hood sash opening which was much more stable than trying to measure low pressure differences that could be affected by someone walking by the hood or in this case walking by the patient.  To also make this work the Phoenix controls systems had to be able to vary flow rates over a 10 to 1 range in less than a second but that was only possible using a special venturi airflow control valve that I helped to develop. Such a device would be overkill and relatively expensive for this application.
>
> My recommendation then is to keep it simple. I would recommend a variable (manually set) control of a fixed flow rate. There will then be a variable velocity through openings and variation in pressure drop but that is okay within reason. Hospital isolation rooms and lab rooms are designed this way with a fixed offset airflow that enters the room. There is a minimum opening such as from a door undercut so that the velocities and pressures do not get to large. In this situation with the enclosure if that becomes a concern because for example the openings become too small or the canopy is fitting to tightly, a good solution is to create a fixed opening with a slight pressure drop across it into the COVID enclosure through some filter material for example. If too difficult to engineer, alternatively some bypass air could come from the room into the exhaust duct so that normally most of the airflow will go through the patient COVID box but if to tight the air will instead come more from the bypass opening. This opening will always pull some air from the room but that is not a bad thing as it helps to filter the room air to filter out any particles that might have escaped into the room for one reason or another.
>
> Anyway, these are some quick top of mind suggestions based on the little I picked up in the zoom meeting today. Perhaps I may have misunderstood some of your planning and thoughts on this. In any case they are hopefully of at least some limited help and if you want to discuss further, let me know
>
> Regards,
>
> Gordon

> Zach,
>
> I’m no airflow expert, but I agree with everything Gordon’s mentioned below. We didn’t have a smoke wand when we were in the sim lab on Friday night, but used a fine streamer to measure directional flow (and provide gestalt regarding velocity) with various size and number of openings in 3 different raw ‘box’ designs and the “Sharp Unit” pulled air across all openings in all the designs.
>
> Look at the material in https://drive.google.com/open?id=1aY_fPkj43AR_9_70Z6bJF7sjg54vLDsv particularly the 2020CDC_IsolationDesign and OK DOH Design files, particularly materials pertaining to ventilated headboard which are featured extensively in this paper, one image being on page 55 as listed on bottom of PDF (pg 67/197), a portable unit being figure 25, page 80 (92/167). They were able to achieve adequate flow/filtration with this wide open design, 8 ft sq open area. If we narrowed down the open area even a little we could drastically diminish our flow requirements (they measured 16 air exchanges/h at 240 cfm).
>
> I like the portability of the unit in figure 25. It would need to be a bit taller. I think that having an enclosure where one could access the patient from many angles through opening flaps (if rigid design) or reaching under flexible plastic sheeting would be advantageous. This would be the most versatile design.
>
> I’ll also elaborate on my “modular” comment from last night. If the ventilation unit could be connected to one of several designs, that would extend its application and down the line one could design an enclosure that would work for an EMS gurney/stretcher.
>
> Regards,
>
> willie

> Hi Zach,
>
> Couple thoughts on the integrated box/fan:
>
> Infection control:
> The unit that’s in immediate proximity to the patient (the isolation box/drape/hood) itself should be amenable to disinfection and/or have disposable related component(s). So, having an integrated ventilation/isolation unit might be more challenging from that perspective. Some of those concerns were woven into the Sharp Unit design. The flexible vent tubing from the box to the unit is inexpensive enough to be disposable and the construct of the filter is such that that entire end of the box (connection fitting included) is also disposable. This version 1 has the filter/fitting end of the unit glued/taped to the ventilation unit, but one could design it so that it clamps on. We have concerns that a drop in filter would be unacceptable from the perspective of flow around the filter.
>
> It might be worthwhile to on-board some hospital infection control folk to provide input early in the design process.
>
> Weight
> Moving the mass of the filter/fan unit remote from the isolation unit allows for a lighter design to the isolation unit.
>
> Versatility
> Having the filter/fan unit remote provides potential to design several types of isolation units to work with one type of filter/fan unit, can target various needs that are difficult to manage with a single isolation box/hood/canopy design
>
> willie

> Zach:
>
> I have about 80 of these 12 VDC 140 mm fans and am happy to give you some if that would be easier and I can get them to you. These fans are best controlled by a pulse width modulated signal running at 20 to 25 kHz. I also have one more small analog board with a pot on it that can be used to generate this signal that you can have as well. Alternatively, you can of course generate these signals from various sources, even an Arduino programmed to do this.
>
> Having some indication of proper flow so that people know the unit is working would be good, perhaps using the pressure sensor to measure velocity pressure of the air flow. Measuring the pressure drop across the fan would be useful if the filter is intended to be used for a while so the gradual buildup of particles and blockage of the filter could be detected. This pressure drop is much larger than the velocity pressure so it will also be an easier means to get at least an indication of flow. If it will be thrown away more frequently then it will not be as much of a need there to measure filter loading. Additionally, if velocity pressure or flow is directly measured this will also be an indication of filter loading or blockage.
>
> Also be aware that one issue with small, high power inline fans is acoustic noise. To get the required flow and pressure they run at very high speeds in some case up to 15,000 rpm. The 140 mm fan that I use run at 7600 rpm at full speed with about 70 dBa of noise generation with a high tonal component. At full speed this frequency component is about 630 Hz. This is also an issue for my work with my kinetic sculptures so I am working to employ a variety of means to reduce this noise such as acoustic noise absorption with some 1” acoustic foam as well as plastic acoustic resonators that use passive noise reduction utilizing an old patented design of mine that I used with my airflow control valves. Perhaps we can reduce the speed if we don’t need the full flow of the fans as running at lower speeds will also significantly reduce the acoustic noise generation.  With this noise in mind, putting these fan filter units close to the patient’s head instead of on the floor will aggravate this issue and create discomfort for the patient.
>
> As noted above I have a “significant” amount of this one inch thick special acoustic absorbing foam with an adhesive backing. I would be happy to donate a couple of the 2 by 4.5’ sheets of this that I have for you to work with. For contamination reasons this would be best applied after the filter. Before the filter we could use one of my tuned acoustic resonator designs as these use no foam and are made from either sheet metal or plastic. They are best tuned for use at a specific speed and are sized for  a quarter wavelength of the frequency to be cancelled.
>
> Regards,
>
> Gordon

Riley Kolus from BC helped consolidate design requirements from his discussions with numerous practitioners:

- must-haves
    - Robust attachment to the bed, patient, or an object to prevent movement relative to the patient.
    - Adequate space inside the container to allow the patient to sit up and move their upper body around a little bit.
    - Access for tubes and wires that need to connect to the patient’s head/neck/torso. These might just go under the box, but should be accessible to nurses.  This has to include room for a BiPAP mask.
    - Must be clear so RN/MD can observe the patient and so the patient can watch tv/avoid claustrophobia.
- luxuries
    - Patients can move their arms inside the box to facilitate eating/using a cell phone.
    - Access+visibility from the back and front for intubation.
    - Access+visibility from the back and both sides for central line placement.
    - Access+visibility from the right side for surgical airway procedures (may require two-handed access).

## other efforts
Harvard's GSD is working on a [similar concept](https://www.gsd.harvard.edu/2020/04/gsd-begins-patient-isolation-hood-pih-design-and-fabrication-alongside-ongoing-ppe-efforts/): a disposable folded box and a negative pressure system to reduce aerosol risk.

A public group [called PreVent](https://drive.google.com/drive/folders/1IQj04TAOrpxp02VxtnU-UU3iDlftDINP?usp=sharing), based out of Cleveland, is also working on a similar effort. They plan to extend the concept to a portable isolation hood for ambulatory patients.
