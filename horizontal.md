## prototype details
This prototype is intended to validate simulation results and represents the first complete iterative spiral around the negative pressure box concept. Importantly, this model will answer a fundamental question about real-world airflow requirements; our initial simulations and expert design consultations (below) revealed a range of airflow specifications that spanned two orders of magnitude (~3 CFM - ~240 CFM). The fan used in this model splits the difference, topping out at ~90 CFM with substantial speed adjustment capability.

The design differs dramatically from other concepts in a few ways:
- the box itself is an innovative folded and latched design, which is lightweight, easy to ship and store, low cost, and disposable.
- the fan and filter assembly are integrated with the box to save cost and space.
- sophisticated electronic controls are used to monitor and control the system.

The upper section that holds the fan and electronics is cut and creased on our Zund using off-cuts from the larger Covid Isolation Box enclosure, then snapped together as shown here:

![fanprototype4](img/fanprototype4.jpg)

A [Bosch 6055C HEPA cabin air filter](https://www.amazon.com/Bosch-6055C-HEPA-Cabin-Filter/dp/B01JYSX028) is adhered using a generous silicone bead to the top of the Covid Isolation Box, which now has a matching rectangular cutout to accommodate the filter:

![fanprototype3](img/fanprototype3.jpg)

The cupola then fits neatly around the filter:

![fanprototype2](img/fanprototype2.jpg)

![fanprototype1](img/fanprototype1.jpg)

During operation, the vacuum created by the fan securely holds the cupula assembly in place. Optionally, this piece can be taped or glued in place. Note that the critical leak path which leads around the HEPA filter is carefully sealed using silicone; other ingress paths, such as the snap assembly holes, caused negligible losses in efficiency and do not increase risk.
