# Electronics

The holes next to the fan cutout will support a circuit board which will include a 24 VDC power jack, a status LED, a differential pressure sensor with extension tube and snubber, a buck converter, a USB debug/logging port, an on/off switch, and a SAMD11 microcontroller:

![fancontrol_schematic](img/fancontrol_schematic.png)

![fancontrol_parts](img/fancontrol_parts.jpg)

The differential pressure sensor will measure the vacuum inside the cupola, using the extension tube and snubber to reduce turbulence effects from the fan. This measurement will be used to validate simulation results at different fan speeds, and to quantify the effects of different shroud configurations around the patient's torso and caregiver access port. If necessary, this circuitry could remain in place to provide an alert if airflow conditions are no longer sufficient to evacuate aerosols from the interior of the box; ideally, the eventual version will simplify this portion to a simple fixed speed circuit to reduce cost and complexity.

Next steps:
- finish electronics design, construction, and programming.
- update simulation with detailed box model, fan flow rate, and filter.
- smoke wand, anemometer, and differential pressure testing to validate simulation results.
- iterate as needed.
