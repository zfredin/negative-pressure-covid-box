# Vertical Negative Pressure box

Last meeting BMU Doctors commented us the need of having a version of the negative pressure box for patients that could be seated.

We decided to build a disposable PETG isolation box for that propose with a completely new architecture.


<img src="img/cadvertical.png" alt="LAttice" width="900" >


The design is based in a curve creased technique called Smooth Gluing. Given two arbitrary 1 curvature surfaces, even if they have taper, its intersection will  be foldable in two individual pieces as they share length of perimeter, given by its intersection.

Then I did the proper Zund layout and added some curved creases on the chest to provide stability and a clamping forze that make the helmet much more stable.

<img src="img/verticalrhino.png" alt="LAttice" width="900" >



The fabrication were straight forward and it was so cool to see how the perimeter matches perfectly in two pieces with completely different curvature. But they a geometrical place on their perimeter!
<img src="img/vertival1.jpeg" alt="LAttice" width="600" >
<img src="img/caoticgif.gif" alt="LAttice" width="300" >


